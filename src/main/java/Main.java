import java.util.List;

import org.mik.zoo.livingbeing.AbstractLivingbeing;
import org.mik.zoo.livingbeing.Livingbeing;
import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.animal.Animal;
import org.mik.zoo.livingbeing.animal.bird.AbstractBird;
import org.mik.zoo.livingbeing.animal.bird.Bird;
import org.mik.zoo.livingbeing.animal.fish.AbstractFish;
import org.mik.zoo.livingbeing.animal.fish.Fish;
import org.mik.zoo.livingbeing.animal.mammal.AbstractMammal;
import org.mik.zoo.livingbeing.animal.mammal.Mammal;
import org.mik.zoo.livingbeing.db.Dao;
import org.mik.zoo.livingbeing.db.ZooDao;
import org.mik.zoo.livingbeing.plant.AbstractPlant;
import org.mik.zoo.livingbeing.plant.Plant;
import org.mik.zoo.livingbeing.plant.flower.AbstractFlower;
import org.mik.zoo.livingbeing.plant.flower.Flower;
import org.mik.zoo.livingbeing.plant.tree.AbstractTree;
import org.mik.zoo.livingbeing.plant.tree.Tree;

/* nev: Farkas Balázs
	neptun: szney8
 */
public class Main {

    private Dao dao;

    public Main() {
    }

    public void process() {
        try {
            this.dao = ZooDao.getInstance();
            this.dao.getAll();
            List<Livingbeing> livingbeings = AbstractLivingbeing.getAllLivingbeings();
            List<Animal> animals = AbstractAnimal.getAllAnimals();
            List<Mammal> mammals = AbstractMammal.getAllMammals();
            List<Fish> fishes = AbstractFish.getAllFishes();
            List<Bird> birds = AbstractBird.getAllBirds();
            List<Plant> plants = AbstractPlant.getAllPlants();
            List<Flower> flowers = AbstractFlower.getAllFlowers();
            List<Tree> trees = AbstractTree.getAllTrees();

            System.out.println(String.format("There are %d livingbeigns, %d animals, %d mammals, %d fishes, %d birds in our zoo",
                    livingbeings.size(), animals.size(), mammals.size(), fishes.size(), birds.size()));
            System.out.println("Birds:");
            for (Bird b : birds)
                System.out.println(b);

            System.out.println("Fishes:");
            for (Fish f : fishes)
                System.out.println(f);

            System.out.println("Mammals:");
            for (Mammal m : mammals)
                System.out.println(m);

            System.out.println("Livingbeings:");
            for (Livingbeing l : livingbeings)
                System.out.println(l);


            System.out.println(String.format("There are %d livingbeigns, %d plants, %d flower, %d tree in our zoo",
                    livingbeings.size(), plants.size(), flowers.size(), trees.size()));

            System.out.println("Flowers:");
            for (Flower f : flowers)
                System.out.println(f);

            System.out.println("Trees:");
            for (Tree t : trees)
                System.out.println(t);

            System.out.println("Livingbeings:");
            for (Livingbeing l : livingbeings)
                System.out.println(l);

        } finally {
            this.dao.close();
        }
    }

    public static void main(String[] args) {
        new Main().process();

    }

}
