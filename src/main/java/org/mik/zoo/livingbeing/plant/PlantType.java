/* nev: Farkas Balázs
	neptun: szney8
 */
package org.mik.zoo.livingbeing.plant;

public enum PlantType {
    TREE(0),
    FLOWER(1),
    UNKNOWN(3);

    private int value;

    PlantType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    public static PlantType fromInt(int val) {
        switch (val) {
            case 0:
                return TREE;
            case 1:
                return FLOWER;
            default:
                return UNKNOWN;
        }
    }
}
