/* nev: Farkas Balázs
	neptun: szney8
 */
package org.mik.zoo.livingbeing.plant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mik.zoo.livingbeing.AbstractLivingbeing;

public abstract class AbstractPlant extends AbstractLivingbeing implements Plant {

    private static List<Plant> allPlants = new ArrayList<>();
    private static Map<PlantType, List<Plant>> mapsByType = new HashMap<>();

    private int maxAge;
    private int maxHeight;
    private PlantType plantType;

    public AbstractPlant() {
    }

    public AbstractPlant(Integer id, String scientificName, String instanceName, String imageURI, int maxAge,
                         int maxHeight, PlantType plantType) {
        super(id, scientificName, instanceName, imageURI);
        this.maxAge = maxAge;
        this.maxHeight = maxHeight;
        this.plantType = plantType;
    }

    public AbstractPlant(ResultSet rs) throws SQLException {
        super(rs);
        this.plantType = PlantType.fromInt(rs.getInt(COL_PLANT_TYPE));
        this.maxAge = rs.getInt(COL_MAX_AGE);
        this.maxHeight = rs.getInt(COL_MAX_HEIGHT);
        registerPlant(this);
    }

    public static void registerPlant(Plant plant) {
        if (!allPlants.contains(plant))
            allPlants.add(plant);

        List<Plant> p = mapsByType.get(plant.getPlantType());

        if (p == null) {
            p = new ArrayList<>();
            mapsByType.put(plant.getPlantType(), p);
        }

        if (!p.contains(plant))
            p.add(plant);
    }

    /**
     * @return the allPlants
     */
    public static List<Plant> getAllPlants() {
        return allPlants;
    }

    /**
     * @param allPlants the allPlants to set
     */
    public static void setAllPlants(List<Plant> allPlants) {
        AbstractPlant.allPlants = allPlants;
    }

    /**
     * @return the mapsByType
     */
    public static Map<PlantType, List<Plant>> getMapsByType() {
        return mapsByType;
    }

    /**
     * @param mapsByType the mapsByType to set
     */
    public static void setMapsByType(Map<PlantType, List<Plant>> mapsByType) {
        AbstractPlant.mapsByType = mapsByType;
    }

    /**
     * @return the maxAge
     */
    @Override
    public int getMaxAge() {
        return this.maxAge;
    }

    /**
     * @param maxAge the maxAge to set
     */
    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    /**
     * @return the maxHeight
     */
    @Override
    public int getMaxHeight() {
        return this.maxHeight;
    }

    /**
     * @param maxHeight the maxHeight to set
     */
    public void setMaxHeight(int maxHeight) {
        this.maxHeight = maxHeight;
    }

    /**
     * @return the plantType
     */
    @Override
    public PlantType getPlantType() {
        return this.plantType;
    }

    /**
     * @param plantType the plantType to set
     */
    public void setPlantType(PlantType plantType) {
        this.plantType = plantType;
    }

    @Override
    public boolean isPlant() {
        return true;
    }

    public static void setColumnDefinitions() {
        columnDefinitions.put(COL_PLANT_TYPE, COL_TYPE_INTEGER);
        columnDefinitions.put(COL_MAX_AGE, COL_TYPE_INTEGER);
        columnDefinitions.put(COL_MAX_HEIGHT, COL_TYPE_INTEGER);
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        super.getInsertSql(fields, values);
        fields.append(',').append(COL_PLANT_TYPE).append(',').append(COL_MAX_AGE).append(',').append(COL_MAX_HEIGHT);
        values.append(',').append(getPlantType().getValue()).append(',').append(getMaxAge()).append(',').append(getMaxHeight());
    }

    @Override
    public String getUpdateSql() {
        return String.format("%s, %s = %d, %s = %d, %s = %d",  //$NON-NLS-1$
                super.getUpdateSql(),
                COL_PLANT_TYPE, Integer.valueOf(getPlantType().getValue()),
                COL_MAX_AGE, Integer.valueOf(this.getMaxAge()),
                COL_MAX_HEIGHT, Integer.valueOf(this.getMaxHeight()));
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + this.maxAge;
        result = prime * result + this.maxHeight;
        result = prime * result + ((this.plantType == null) ? 0 : this.plantType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof AbstractPlant))
            return false;
        AbstractPlant other = (AbstractPlant) obj;
        if (this.maxAge != other.maxAge)
            return false;
        if (this.maxHeight != other.maxHeight)
            return false;
        if (this.plantType != other.plantType)
            return false;
        return false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return super.toString() + ", maxAge=" + this.maxAge  //$NON-NLS-1$
                + ", maxHeight=" + this.maxHeight  //$NON-NLS-1$
                + ", plantType=" + this.plantType; //$NON-NLS-1$
    }
}
