/* nev: Farkas Balázs
	neptun: szney8
 */
package org.mik.zoo.livingbeing.plant.flower;

import org.mik.zoo.livingbeing.Color;
import org.mik.zoo.livingbeing.plant.PlantType;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Tulip extends AbstractFlower {
	
    public static final String SCIENTIFIC_NAME = "Tulip"; //$NON-NLS-1$
    public static final String IMAGE_URI = "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/%D7%A6%D7%91%D7%A2%D7%95%D7%A0%D7%99%D7%9D.JPG/1920px-%D7%A6%D7%91%D7%A2%D7%95%D7%A0%D7%99%D7%9D.JPG"; //$NON-NLS-1$
    private static final int NUMBER_OF_BUDS = 2;
    private static final int MAX_AGE = 7;
    private static final int MAX_HEIGHT = 4;

	public Tulip() {
	}

	public Tulip(Integer id, String scientificName, String instanceName, String imageURI, int maxAge, int maxHeight,
				 PlantType plantType, int numberOfBuds, Color colorOfBuds) {
		super(id, scientificName, instanceName, imageURI, maxAge, maxHeight, plantType, numberOfBuds, colorOfBuds);
	}

	public Tulip(String instanceName) {
		super(null, SCIENTIFIC_NAME, instanceName, IMAGE_URI, MAX_AGE, MAX_HEIGHT, PlantType.FLOWER, NUMBER_OF_BUDS, Color.MULTIPLE);
	}

	public Tulip(String instanceName, Color color) {
		super(null, SCIENTIFIC_NAME, instanceName, IMAGE_URI, MAX_AGE, MAX_HEIGHT, PlantType.FLOWER, NUMBER_OF_BUDS, color);
	}


	public Tulip(ResultSet rs) throws SQLException {
		super(rs);
		this.setColorOfBuds(Color.fromInt(rs.getInt(COL_COLOR_OF_BUDS)));
	}
	
	public static Tulip createFromResultSet(ResultSet rs) throws SQLException {
		return new Tulip(rs);
	}
	
	@Override
	public String toString() {
		return "Tulip:" + super.toString(); //$NON-NLS-1$
	}


}
