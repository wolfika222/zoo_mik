/* nev: Farkas Balázs
	neptun: szney8
 */
package org.mik.zoo.livingbeing.plant.flower;

import org.mik.zoo.livingbeing.Color;
import org.mik.zoo.livingbeing.plant.AbstractPlant;
import org.mik.zoo.livingbeing.plant.PlantType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractFlower extends AbstractPlant implements Flower {
	private static List<Flower> allFlowers = new ArrayList<>();

	private int numberOfBuds;
	private Color colorOfBuds;

	public AbstractFlower() {
	}

	public AbstractFlower(Integer id, String scientificName, String instanceName, String imageURI, int maxAge,
                          int maxHeight, PlantType plantType, int numberOfBuds, Color colorOfBuds) {
		super(id, scientificName, instanceName, imageURI, maxAge, maxHeight, plantType);
		this.numberOfBuds = numberOfBuds;
		this.colorOfBuds = colorOfBuds;
	}

	public AbstractFlower(ResultSet rs) throws SQLException {
		super(rs);
		this.numberOfBuds = rs.getInt(COL_NUMBER_OF_BUDS);
		this.colorOfBuds = Color.fromInt(rs.getInt(COL_COLOR_OF_BUDS));
		registerFlower(this);
	}

	/**
	 * @return the allFlowers
	 */
	public static List<Flower> getAllFlowers() {
		return allFlowers;
	}

	/**
	 * @param allFlowers the allFlowers to set
	 */
	public static void setAllFlowers(List<Flower> allFlowers) {
		AbstractFlower.allFlowers = allFlowers;
	}

	/**
	 * @return the numberOfBuds
	 */
	@Override
	public int getNumberOfBuds() {
		return this.numberOfBuds;
	}

	/**
	 * @param numberOfBuds the numberOfBuds to set
	 */
	public void setNumberOfBuds(int numberOfBuds) {
		this.numberOfBuds = numberOfBuds;
	}

	/**
	 * @return the colorOfBuds
	 */
	@Override
	public Color getColorOfBuds() {
		return this.colorOfBuds;
	}

	/**
	 * @param colorOfBuds the colorOfBuds to set
	 */
	public void setColorOfBuds(Color colorOfBuds) {
		this.colorOfBuds = colorOfBuds;
	}

	@Override
	public boolean isFlower() {
		return true;
	}

	@Override
	public String getUpdateSql() {
		return String.format("%s, %s=%d, %s=%d", super.getUpdateSql(), //$NON-NLS-1$
				COL_NUMBER_OF_BUDS, Integer.valueOf(getNumberOfBuds()), COL_COLOR_OF_BUDS,
				Integer.valueOf(getColorOfBuds().getValue()));
	}

	@Override
	public void getInsertSql(StringBuilder fields, StringBuilder values) {
		super.getInsertSql(fields, values);
		fields.append(',').append(COL_NUMBER_OF_BUDS).append(',').append(COL_COLOR_OF_BUDS);
		values.append(',').append(this.getNumberOfBuds()).append(',').append(this.getColorOfBuds().getValue());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.colorOfBuds == null) ? 0 : this.colorOfBuds.hashCode());
		result = prime * result + this.numberOfBuds;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof AbstractFlower))
			return false;
		AbstractFlower other = (AbstractFlower) obj;
		if (this.colorOfBuds != other.colorOfBuds)
			return false;
		if (this.numberOfBuds != other.numberOfBuds)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return super.toString() + ", numberOfBuds=" + this.numberOfBuds + ", colorOfBuds=" + this.colorOfBuds; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	public static void setColumnDefinitions() {
		columnDefinitions.put(COL_NUMBER_OF_BUDS, COL_TYPE_VARCHAR_255);
		columnDefinitions.put(COL_COLOR_OF_BUDS, COL_TYPE_INTEGER);
	}

	public static void registerFlower(Flower flower) {
		if (!allFlowers.contains(flower))
			allFlowers.add(flower);
	}

}
