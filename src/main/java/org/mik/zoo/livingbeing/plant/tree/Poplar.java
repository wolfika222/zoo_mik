/* nev: Farkas Balázs
	neptun: szney8
 */
package org.mik.zoo.livingbeing.plant.tree;

import org.mik.zoo.livingbeing.plant.PlantType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Poplar extends AbstractTree {
	
    public static final String SCIENTIFIC_NAME = "Poplar"; //$NON-NLS-1$
    public static final String IMAGE_URI = "https://www.jungseed.com/PIF/22432/ThevesPoplar.jpg"; //$NON-NLS-1$
    private static final int MAX_AGE = 100;
    private static final int MAX_HEIGHT = 195;
    private static final float BARK_THICKNESS = (float) 2.9;


	public Poplar() {
		super(null, SCIENTIFIC_NAME, "", IMAGE_URI, MAX_AGE, MAX_HEIGHT, PlantType.TREE, BARK_THICKNESS); //$NON-NLS-1$
	}

	public Poplar(Integer id, String scientificName, String instanceName, String imageURI, int maxAge, int maxHeight,
				  PlantType plantType, float barkThickness) {
		super(id, scientificName, instanceName, imageURI, maxAge, maxHeight, plantType, barkThickness);
	}

	public Poplar(ResultSet rs) throws SQLException {
		super(rs);
	}
	
	public Poplar(String instanceName) {
		super(null, SCIENTIFIC_NAME, instanceName, IMAGE_URI, MAX_AGE, 
				MAX_HEIGHT, PlantType.TREE, BARK_THICKNESS);
	}
	
	public static Poplar createFromResultSet(ResultSet rs) throws SQLException {
		return new Poplar(rs);
	}
	
	@Override
	public String toString() {
		return "Poplar:" + super.toString(); //$NON-NLS-1$
	}

}
