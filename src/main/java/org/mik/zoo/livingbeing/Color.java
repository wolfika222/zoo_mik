/* nev: Farkas Balázs
	neptun: szney8
 */
package org.mik.zoo.livingbeing;

public enum Color {

    BLACK(0, "black"),
    WHITE(1, "white"),
    RED(2, "red"),
    GREEY(3, "grey"),
    ORANGE(4,"orange"),
    MULTIPLE(4, "multiple");

    private int value;
    private String name;

    public int getValue() {
        return this.value;
    }

    Color(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public static Color fromInt(int value) {
        switch(value) {
            case 0: return BLACK;
            case 1: return WHITE;
            case 2: return RED;
            case 3: return GREEY;
            case 4: return ORANGE;
            default:
                return MULTIPLE;
        }
    }
}
