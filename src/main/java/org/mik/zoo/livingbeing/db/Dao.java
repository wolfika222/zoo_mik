/* nev: Farkas Balázs
	neptun: szney8
 */
package org.mik.zoo.livingbeing.db;

import org.mik.zoo.livingbeing.Livingbeing;

import java.util.List;


public interface Dao {
	
	void close();

    Livingbeing getById(Integer id);

    List<Livingbeing> getAll();

    List<Livingbeing> findByScientificName(String name);

    List<Livingbeing> findByInstanceName(String name);

    boolean delete(Livingbeing lb);

    Livingbeing persist(Livingbeing lb);
}
